﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gamemanagerscript : MonoBehaviour {

    public static gamemanagerscript instance = null;

    public int songchoice;
    public Queue<notedatastruct> bflatscale, ridingthe4thwave;
    public List<Queue<notedatastruct>> songcatalog;
    public Transform start, end;
    public Vector3 b, a, g, f, e, d, c;
    public TH_NoteSong noteprefab;
    private int score;
    public int notespassed {
        get { return score; }
        set { onscorechanged(value);
            score = value; }
    } 
    public int notesfailed;
    public delegate void scorechangedevent(int score);
    public scorechangedevent onscorechanged;

    //song catalog


    // Use this for initialization
    void Awake()
    {

        if (instance == null)
        {

            instance = this;

        }
        else if (instance != this)
        { 
            Destroy(gameObject);
        }   
        
    }

    private void Start()
    {
        notespassed = 0;
        notesfailed = 0;
        definenotepositions();
        //populatebflatscale();
        populateRidingthe4thWave();
        populatesongcatalog();
    }

    // Update is called once per frame
    void Update () {
		if (((float)notespassed / (float)notesfailed) < 0.15)  //fail condition
        {
            
        }
	}

    void populatesongcatalog() //add all currently defined songs to the master song list here
    {
        songcatalog = new List<Queue<notedatastruct>>(); 
        songcatalog.Add(ridingthe4thwave);
    }

    void populatebflatscale()  //0 in the catalog 
    {
        bflatscale = new Queue<notedatastruct>();

        notedatastruct notetemp;

        notetemp = new notedatastruct(1f, 5f, b);
        bflatscale.Enqueue(notetemp);

        notetemp = new notedatastruct(1f, 6f, c);
        bflatscale.Enqueue(notetemp);

        notetemp = new notedatastruct(1f, 7f, d);
        bflatscale.Enqueue(notetemp);

        notetemp = new notedatastruct(1f, 8f, e);
        bflatscale.Enqueue(notetemp);

        notetemp = new notedatastruct(1f, 9f, f);
        bflatscale.Enqueue(notetemp);

        notetemp = new notedatastruct(1f, 10f, g);
        bflatscale.Enqueue(notetemp);

        notetemp = new notedatastruct(1f, 11f, a);
        bflatscale.Enqueue(notetemp);

        notetemp = new notedatastruct(1f, 12f, b);
        bflatscale.Enqueue(notetemp);




    }

    void populateRidingthe4thWave()
    {
        ridingthe4thwave = new Queue<notedatastruct>();

        notedatastruct notetemp;

        notetemp = new notedatastruct((172f / 60f), 37.665f, e); //1
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 38.537f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 38.7114f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 38.8858f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 39.0602f, d); //5
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 39.7578f, b);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 40.4554f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 40.8042f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 41.153f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 41.5018f, b); //10
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 41.8506f, f);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 42.3738f, g);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 42.897f, f);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 43.2458f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 44.1178f, e);  //15
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 44.2922f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 44.4666f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 44.641f, d);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 45.1642f, b);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 45.6847f, a); //20
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 46.035f, b);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 48.825f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 49.3482f, b);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 49.8714f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 50.2202f, d); //25
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 50.7434f, b);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 51.2666f, d);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 51.6154f, c);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 52.1386f, a);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 52.6618f, c); //30
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 53.0106f, d);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 53.5338f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 54.057f, f);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 54.4058f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 55.2778f, e);  //35
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 55.4522f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 55.6266f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 55.8f, d);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 56.3232f, b);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 56.8464f, a); //40
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 57.1952f, b);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 59.2878f, g);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 59.6366f, f);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 59.9854f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 61.0318f, e); //45
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 61.3806f, f);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 62.427f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 62.6014f, f);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 62.7758f, g);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 64.1708f, a); //50
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 64.5196f, g);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 64.8684f, a);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 65.2172f, b);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 65.565f, g);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 65.9138f, e); //55
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 66.6114f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 67.309f, d);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 68.0066f, c);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f),68.3554f, b);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 68.5298f, c);  //60
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 68.7042f, g);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 69.053f, g);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f),69.7506f, a);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 71.145f, d);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 71.3194f, e); //65
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 71.4938f, g);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 71.6682f, d);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f),71.8426f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 72.017f, g);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 72.1914f, d); //70
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 72.3658f, e); 
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 72.5402f, d);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 72.7146f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 72.889f, g);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 73.0634f, d); //75
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 73.2378f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 73.4122f, g);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 73.5866f, d);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 73.935f, a);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 74.1094f, a);  //80
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 74.2838f, a);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 76.0278f, d);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 76.2022f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 76.3766f, d);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 76.725f, a); //85
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 77.4226f, d);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 78.1202f, b);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 79.8642f, g);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 80.213f, f);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 80.3874f, e);  //90
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 80.5618f, d);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 80.7362f, c);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 80.9106f, a);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 81.2594f, f);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 81.6082f, g);  //95
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 81.957f, d);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 82.3058f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 149.265f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 150.137f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 150.3114f, e); //100
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 150.4858f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 150.6602f, d);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 151.3578f, b);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 152.0554f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 152.4042f, e); //105
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 152.753f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 153.1018f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 153.4506f, f);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 153.9741f, g);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 154.4973f, f); //110
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 154.845f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 155.717f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 155.8914f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 156.0658f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 156.2402f, d); //115
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 156.7634f, b);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 157.2866f, a);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 157.635f, b);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 160.425f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 160.9482f, b);  //120
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 161.4714f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 161.8202f, d);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 162.3434f, b);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 162.8666f, d);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 163.2154f, c); //125
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 163.7386f, a);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 164.2618f, c);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 164.6106f, d);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 165.1338f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 165.657f, f); //130
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 166.005f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 166.877f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 167.0514f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 167.2258f, e);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 167.4002f, d); //135
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 167.9234f, b);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 168.4466f, a);
        ridingthe4thwave.Enqueue(notetemp);

        notetemp = new notedatastruct((172f / 60f), 168.7954f, b);
        ridingthe4thwave.Enqueue(notetemp);

    }

    void definenotepositions()
    {
        b = start.position;
        a = start.position;
        g = start.position;
        f = start.position;
        e = start.position;
        d = start.position;
        c = end.position;


        float totaldistance = end.position.x - start.position.x;
        float offset = totaldistance / 6f;

        a.x += offset;
        g.x += 2 * offset;
        f.x += 3 * offset;
        e.x += 4 * offset;
        d.x += 5 * offset;


    }

    public struct notedatastruct
    {
        public Vector3 position;
        public float time, speed;

        public notedatastruct(float newspeed, float newtime, Vector3 newpostion)
        {
            this.position = newpostion;
            this.time = newtime;
            this.speed = newspeed;
        }

        public notedatastruct(notedatastruct newstruct)
        {
            this.position = newstruct.position;
            this.time = newstruct.time;
            this.speed = newstruct.speed;
        }
    }
}
