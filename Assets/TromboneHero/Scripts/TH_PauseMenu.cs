﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GamewingResourceLibrary.MenuSystem;

public class TH_PauseMenu : MonoBehaviour {

	public static bool GameIsPaused = false;
	public static void PauseGame(){
		if(GameIsPaused == false){
			//create Pause Menu
			GameIsPaused = true;
			GameObject go = new GameObject("Pause Menu");
			go.AddComponent<TH_PauseMenu>();
			go.transform.SetParent(THHUDscript.current.transform, false);
		}
	}

	AudioSource songPlayer;

	float prevTimeScale = 1f;
	void Pause(){
		prevTimeScale = Time.timeScale;
		songPlayer.Pause ();
		Time.timeScale = 0;
	}

	void Unpause(){
		songPlayer.UnPause ();
		Time.timeScale = prevTimeScale;
		GameIsPaused = false;
		Destroy(gameObject);
	}

	void Start(){
		songPlayer = GameObject.FindGameObjectWithTag ("SongPlayer").GetComponent<AudioSource>();
		if (songPlayer == null) {
			print("NULL!");
		}

		Pause();
		MakeMenu();
	}

	MenuItemList menu;
	void MakeMenu(){
		menu = MenuItemList.Create<MenuItemList>(new Vector3(Screen.width / 2f, Screen.height / 2f));
		menu.AddItem(MenuItem.Create("Unpause", menu_UnpauseGame));
		menu.AddItem(MenuItem.Create("Return to Main Menu", menu_ReturnToHub));

		menu.transform.SetParent(transform, false);

		foreach(var m in menu.menuItemList){
			m.font.Align_H = DrawBitmapFont.Halign.Center;
			m.font.Align_V = DrawBitmapFont.Valign.Middle;
		}
	}

	void menu_UnpauseGame(MenuItem source, int val){
		Unpause();
	}

	void menu_ReturnToHub(MenuItem source, int val){
		G1_GameManager.Instance.LoadNewScene("Shell");
		menu.DestroyMenuChain();
	}
}
