﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GamewingResourceLibrary;

public class TromboneSlider : MonoBehaviour {


    public Rigidbody tromboneslider;
    public Transform start, end;
    public float movespeed = 15;

    SuperInput.ControllerType controller;
    SuperInput.ControllerNode inputMoveX;

    private float position, moveHorizontal;

    private SuperInput.ControllerType control;

    // Use this for initialization
    void Start () {
        control = GlobalInput.Instance.controller;
        //debug
        
    }

    // Update is called once per frame
    void Update()
    {
        moveHorizontal = control.getValue(0);

        position = Mathf.Clamp(position + moveHorizontal * Time.deltaTime * movespeed, 0f, 1f);

        if(control.getPressedNegative(1))
        {
            position = 1;
        }

        if(control.getPressedPositive(1))
        {
            position = 0;
        }
    }

    private void FixedUpdate()
    {

        Vector3 tempvec = Vector3.Lerp(start.position, end.position, position);
        tromboneslider.MovePosition(tempvec);
        
        


    }
}
