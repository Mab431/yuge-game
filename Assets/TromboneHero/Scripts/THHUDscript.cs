﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class THHUDscript : MonoBehaviour {

	public static THHUDscript current;
    gamemanagerscript g1;
    public DrawBitmapFont scoredisplay;
    
	void Awake(){
		current = this;
	}

    // Use this for initialization
    private void Start()
    {
        g1 = gamemanagerscript.instance;
        g1.onscorechanged += onscorechanged;

    }

    void onscorechanged(int s)
    {
        scoredisplay.Text = "Score: " + s.ToString();
    }
}
