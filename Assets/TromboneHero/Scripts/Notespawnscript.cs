﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GamewingResourceLibrary;

public class Notespawnscript : MonoBehaviour {


    public TH_NoteSong noteprefab;
    public Transform start, end;
    private Queue<gamemanagerscript.notedatastruct> song;
    private float timer;
    private int noteincrement;
    private gamemanagerscript.notedatastruct currentnote;
    private float songtempo;
    private int totalincrement;
    private List<TH_NoteSong> notesonscreen;

	// Use this for initialization
	void Start ()
    {
        timer = 0f;
        noteincrement = 0;
        totalincrement = 0;
        notesonscreen = new List<TH_NoteSong>();

        /* TH_NoteSong notetemp = Instantiate<TH_NoteSong>(noteprefab);
        TH_NoteSong notetemp1 = Instantiate<TH_NoteSong>(noteprefab);
        notetemp.transform.position = start.position;
        notetemp1.transform.position = end.position; */

        song = gamemanagerscript.instance.songcatalog[gamemanagerscript.instance.songchoice];
        
        //currentnote = Instantiate<TH_NoteSong>(noteprefab);

        currentnote = song.Dequeue();
        songtempo = currentnote.speed;

		float dis = 10f;//fixed distance
		timer = dis + songtempo;

        //currentnote.Initialize(0f, currentnote.time, currentnote.transform);

    }
	
	// Update is called once per frame
	void Update ()
    {
		if (GlobalInput.Instance.controller.getPressed (4)) {
			TH_PauseMenu.PauseGame ();
		}

        timer += Time.deltaTime;

        if((currentnote.time) <= timer)
        {
            //currentnote.Initialize(songtempo, currentnote.time, currentnote.transform);
            

            if (song.Count >= 0)
            {
                var n = Instantiate<TH_NoteSong>(noteprefab);
				//n.Initialize(currentnote.speed, 0, currentnote.position);
				n.Initialize(1, 0, currentnote.position);
               

                if (song.Count != 0) {
                    currentnote = song.Dequeue();
                }
                else
                {
                    currentnote.time = float.PositiveInfinity; //end the game code here
                }
            }
            
            
            
            Debug.Log(totalincrement + " made it here: " + timer   );

        }
        

	}

    
}
