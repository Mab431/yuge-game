﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TH_NoteSong : MonoBehaviour {



    public Rigidbody rb;
    public float speed, time;
    


    //todo: add position, look at notebook

   

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        //
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void FixedUpdate()
    {

        
        rb.MovePosition(transform.position + Vector3.down * speed * Time.fixedDeltaTime );
    }

    public void Initialize(float newspeed, float newtime, Vector3 pos)
    {
        speed = newspeed;
        time = newtime;
        transform.position = pos;

    }
}
