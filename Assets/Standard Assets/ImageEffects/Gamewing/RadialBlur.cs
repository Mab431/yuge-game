using System;
using UnityEngine;

namespace UnityStandardAssets.ImageEffects
{
    [ExecuteInEditMode]
    [RequireComponent (typeof(Camera))]
    [AddComponentMenu ("Image Effects/Shaders/RadialBlur")]
    public class RadialBlur : PostEffectsBase
	{
		[Range(-1f,2f)]
		public float PosX = 0.5f;
		[Range(-1f,2f)]
		public float PosY = 0.5f;
        [Range(1, 128)]
		public int Steps = 32;
		[Range(0f,1f)]
		public float Distance = 0.25f;
		[Range(0f,1f)]
		public float Falloff = 1f;
		[Range(0f,1f)]
		public float Intensity = 1f;
		public Texture2D Mask;
		public bool useMask;
		public bool invertMask;


        public Shader RadBlurShader = null;
		private Material RadBlurMat = null;


        public override bool CheckResources ()
		{
            CheckSupport (false);
            RadBlurMat = CheckShaderAndCreateMaterial(RadBlurShader,RadBlurMat);

			/*
            if (!isSupported)
                ReportAutoDisable ();
            return isSupported;
            */
			return true;
        }

        void OnRenderImage (RenderTexture source, RenderTexture destination)
		{
            if (CheckResources()==false)
			{
                Graphics.Blit (source, destination);
                return;
            }

			RadBlurMat.SetVector ("_Position", new Vector4 (PosX, PosY, 0, 0));
			RadBlurMat.SetInt ("_steps", Steps);
			RadBlurMat.SetFloat ("_distance", Distance);
			RadBlurMat.SetFloat ("_intensity", Intensity);
			RadBlurMat.SetFloat ("_falloff", Falloff);
			RadBlurMat.SetTexture ("_mask", Mask);
			if(invertMask){
				RadBlurMat.SetInt ("_invertMask", 1);
			}else{
				RadBlurMat.SetInt ("_invertMask", 0);
			}
			if(useMask){
				RadBlurMat.SetInt ("_useMask", 1);
			}else{
				RadBlurMat.SetInt ("_useMask", 0);
			}
            Graphics.Blit (source, destination, RadBlurMat);
        }
    }
}
