﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Hidden/RadialBlur"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			//variables
			sampler2D _MainTex;
			float2 _Position;
			int _steps;
			float _distance;
			float _intensity;
			float _falloff;
			int _useMask;
			sampler2D _mask;
			int _invertMask;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 original = tex2D(_MainTex, i.uv);
				fixed4 col = float4(0,0,0,1);
				float ofs;
				float maskAlpha;
				float fall = 1;
				float div = 0; //amount to divide by
				for (int x = 0; x < _steps; x++){
					ofs = (-_distance * float(x) / float(_steps));
					fall = max(fall-_falloff/_steps,0);
					col += tex2D(_MainTex, (i.uv*(1+ofs) - _Position * ofs)) * (fall);
					div += fall;
				}
				col /= div;
				if(_useMask){
					maskAlpha = tex2D(_mask, i.uv).r;
					//col /= _steps;
					if(_invertMask){
						col = lerp(col, original, 1 - maskAlpha);
					}else{
						col = lerp(original, col, 1 - maskAlpha);
					}
				}
				col = lerp(original, col,  _intensity);
				return col;
			}
			ENDCG
		}
	}
}
