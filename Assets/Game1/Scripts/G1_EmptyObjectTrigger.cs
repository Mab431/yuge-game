﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class G1_EmptyObjectTrigger : MonoBehaviour
{
    public List<GameObject> ActivateOnEnter = new List<GameObject>();
    public List<GameObject> DeactivateOnEnter = new List<GameObject>();
    public List<GameObject> ActivateAndDestoryOnEnter = new List<GameObject>();
    public float time = 10;

    IEnumerator Acitvate()
    {
        try
        {
            foreach (var g in ActivateOnEnter)
            {
                g.SetActive(true);
            }
        }
        catch (MissingReferenceException e)
        {
            Debug.Log("Object destoryed before it could be set to active");
        }
        yield return null;
    }

    IEnumerator Deactivate()
    {
        try
        {
            foreach (var g in DeactivateOnEnter)
            {
                g.SetActive(false);
            }
        }
        catch (MissingReferenceException e)
        {
            Debug.Log("Object destoryed before it could be set to inactive");
        }
        yield return null;
    }

    IEnumerator ActivateAndDestroy()
    {
        try
        {
            foreach (var g in ActivateAndDestoryOnEnter)
            {
                g.SetActive(true);
            }
        }
        catch(MissingReferenceException e)
        {
            Debug.Log("Object destoryed before it could be set to active");
        }

        yield return new WaitForSeconds(time);

        try
        { 
            foreach (var g in ActivateAndDestoryOnEnter)
            {
                g.SetActive(false);
            }
        }
        catch (MissingReferenceException e)
        {
            Debug.Log("Object destoryed before it could be set to inactive");
        }
    }

    void OnTriggerEnter(Collider col)
    {
        var player = col.gameObject.GetComponent<G1_PlayerCharacter>();

        if (player != null)
        {
            StartCoroutine(Acitvate());
            StartCoroutine(Deactivate());
            StartCoroutine(ActivateAndDestroy());
        }
    }
}