﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Camera))]
public class Game1Camera : MonoBehaviour {

	//Object Refs
	public Transform target;
	Camera _camera;
	public Camera Camera{
		get{ 
			return _camera;
		}
	}

	//public vars
	public float MaxDistance = 6f;
	public float MinDistance = 2f;
	public float CameraSpeed = 10f;
	public bool LockDirection = false;

	//private vars
	Vector3 targetPos;

	// Use this for initialization
	void Start () {
		_camera = GetComponent<Camera> ();
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if (target == null) {
			return;
		}

		//follow
		var dis = Vector3.Distance(target.position, transform.position);
		/*
		if (dis > MaxDistance) {
			targetPos = target.position - transform.forward * MaxDistance;
		}else if(dis < MinDistance){
			targetPos = target.position - transform.forward * MinDistance;
		}
		*/

		targetPos = target.position - transform.forward * MaxDistance;

		transform.position = Vector3.MoveTowards (transform.position, targetPos, Time.deltaTime * CameraSpeed);
	}

	Coroutine radialExplosionCoRoutine = null;
	public void RadialExplosion(float duration, float power, Vector2 pos){
		if(radialExplosionCoRoutine != null){
			StopCoroutine(radialExplosionCoRoutine);
		}
		radialExplosionCoRoutine = StartCoroutine(Co_RadialExplosion (duration, power, pos));
	}

	IEnumerator Co_RadialExplosion(float duration, float power, Vector2 pos){
		UnityStandardAssets.ImageEffects.RadialBlur radblur = GetComponent<UnityStandardAssets.ImageEffects.RadialBlur>();

		float time = 0f;
		float sintime = 0f;

		radblur.enabled = true;
		radblur.PosX = pos.x;
		radblur.PosY = pos.y;

		while(time < duration){

			radblur.Distance = ((time + 0.5f) / (duration + 0.5f)) * power;
			radblur.Intensity = (1 - (time / duration));

			time += Time.deltaTime;
			sintime = Mathf.Sin((time / duration) * Mathf.PI);
			yield return null;
		}

		radblur.enabled = false;
		radialExplosionCoRoutine = null;
	}
}
