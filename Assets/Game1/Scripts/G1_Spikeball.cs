﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class G1_Spikeball : MonoBehaviour {

	Rigidbody rigidBody;
	public int Damage = 1;
	public GameObject Fire;

	// Use this for initialization
	void Start () {
		rigidBody = GetComponent<Rigidbody>();
	}

	void OnTriggerEnter(Collider col){
		G1_CharacterBase c = col.gameObject.GetComponent<G1_CharacterBase>();
		if(c == null){
			return;
		}

		c.ApplyDamage(Damage, G1_CharacterBase.DamageType.Damage);
		var g = Instantiate(Fire, transform.position, Quaternion.Euler(0,0,0));
		Destroy(g, 2f);
		Destroy(gameObject);
	}
}
