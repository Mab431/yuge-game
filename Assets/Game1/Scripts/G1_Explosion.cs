﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class G1_Explosion : MonoBehaviour {

	public float ExplosionForce = 20f;
	public int ExplosionDamage = 2;
	public float ExplosionRange;
	public AnimationCurve Falloff;

	public bool AffectRigidBodies = false;
	public bool AffectCharacters = true;
	public bool RayCheck = false;

	ParticleSystem particles;
	float effectRange = 4f;

	// Use this for initialization
	void Start () {
		particles = GetComponent<ParticleSystem>();

		Explode();
	}
	
	// Update is called once per frame
	void Update () {
		if(!particles.IsAlive()){
			Destroy(gameObject);
		}
	}

	void Explode(){
		//physics objects
		if(AffectRigidBodies){
			var list = Physics.OverlapSphere(transform.position, ExplosionRange);
			foreach (var p in list){
				float dis = Vector3.Distance(transform.position, p.transform.position);
				if(dis <= ExplosionRange){
					float disNormal = 1 - (dis / ExplosionRange);
					int damage = Mathf.RoundToInt (ExplosionDamage * disNormal);

				}
			}
		}
		//characters
		if(AffectCharacters){
			var list = GameObject.FindObjectsOfType<G1_CharacterBase>();
			foreach (var c in list){
				float dis = Vector3.Distance(transform.position, c.transform.position);
				if(dis <= ExplosionRange){
					float disNormal = 1 - (dis / ExplosionRange);
					int damage = Mathf.RoundToInt (ExplosionDamage * disNormal);
					c.ApplyDamage(damage, G1_CharacterBase.DamageType.Damage);
					c.ApplyForce((c.transform.position - transform.position).normalized * disNormal * ExplosionForce);
				}
			}
		}

		Vector2 screenSpace = new Vector2();
		foreach(var c in GameObject.FindObjectsOfType<Game1Camera>()){
			float dis = Vector3.Distance (c.transform.position, transform.position);
			if (dis <= ExplosionRange * effectRange) {
				Vector3 screen = c.Camera.WorldToViewportPoint (transform.position);
				screenSpace.x = screen.x;
				screenSpace.y = screen.y;

				c.RadialExplosion (0.5f, (1 - ((dis) / (ExplosionRange * effectRange))) * effectRange, screenSpace);
			}
		}
	}
}
