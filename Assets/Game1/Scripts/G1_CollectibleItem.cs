﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class G1_CollectibleItem : MonoBehaviour {

    public G1_CollectibleSystem collectible;
	public int ScoreValue = 100;

	public Transform ObjectToRotate;
	public float RotationSpeed;

	void Update(){
		if(ObjectToRotate != null){
			ObjectToRotate.RotateAround(ObjectToRotate.position, Vector3.up, RotationSpeed * Time.deltaTime);
		}
	}

    void OnTriggerEnter(Collider col)
    {
        G1_CharacterBase player = col.gameObject.GetComponent<G1_CharacterBase>();
        if (player == null)
        {
            return;
        }

		if(collectible != null){
        	collectible.itemCollected(this);
		}

		G1_GameManager.Instance.CurrentScore += ScoreValue;
        Destroy(gameObject);
    }
}
