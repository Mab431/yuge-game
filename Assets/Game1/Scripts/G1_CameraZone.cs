﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class G1_CameraZone : MonoBehaviour {

	public float Angle;
	public float Tilt;
	public float time;
	public bool UseAngle = true;
	public bool UseTilt = true;
	public bool RevertOnLeave = false;

	private Coroutine transition;
	Collider lastCollider;

	float prevAngle;
	float PrevTilt;

	IEnumerator co_transition (Camera cam, Transform pos, float angle, float tilt) {
		float t = 0;
		Vector3 start = cam.transform.eulerAngles;

		float startX = start.x;
		float startY = start.y;

		float dis = Vector3.Distance (cam.transform.position, pos.position);


		Vector3 cameraAngle = start;
		while (t < time) {
			if (UseTilt) {
				cameraAngle.x = Mathf.LerpAngle (start.x, tilt, t / time);
			}
			if (UseAngle) {
				cameraAngle.y = Mathf.LerpAngle (start.y, angle, t / time);
			}
			cam.transform.eulerAngles = cameraAngle;
			cam.transform.position = pos.position - cam.transform.forward * dis;

			t += Time.deltaTime;
			yield return null;
		}
		if (UseTilt) {
			cameraAngle.x = tilt;
		}
		if (UseAngle) {
			cameraAngle.y = angle;
		}

		cam.transform.eulerAngles = cameraAngle;
		cam.transform.position = pos.position - cam.transform.forward * dis;

	}

	void OnTriggerEnter (Collider col) {
		var player = col.gameObject.GetComponent<G1_PlayerCharacter> ();

		if (player != null) {
			if (col != lastCollider) {
				lastCollider = col;
				if (transition != null) {
					StopCoroutine (transition);
				}
				prevAngle = player.cam.transform.eulerAngles.y;
				PrevTilt = player.cam.transform.eulerAngles.x;
				transition = StartCoroutine (co_transition (player.cam, player.transform, Angle, Tilt));
			}
		}
	}

	void OnTriggerExit(Collider col){
		if(RevertOnLeave == false){
			return;
		}

		var player = col.gameObject.GetComponent<G1_PlayerCharacter> ();

		if (player != null) {
			lastCollider = col;
			if (transition != null) {
				StopCoroutine (transition);
			}
			transition = StartCoroutine (co_transition (player.cam, player.transform, prevAngle,PrevTilt));
		}
	}
}