﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GamewingResourceLibrary.MenuSystem;

public class G1_PauseMenu : MonoBehaviour {

	public static bool GameIsPaused = false;
	public static void PauseGame(){
		if(GameIsPaused == false){
			//create Pause Menu
			GameIsPaused = true;
			GameObject go = new GameObject("Pause Menu");
			go.AddComponent<G1_PauseMenu>();
			go.transform.SetParent(G1_HUD.Current.transform, false);
		}
	}

	float prevTimeScale = 1f;
	void Pause(){
		prevTimeScale = Time.timeScale;
		Time.timeScale = 0;
	}

	void Unpause(){
		Time.timeScale = prevTimeScale;
		GameIsPaused = false;
		Destroy(gameObject);
	}

	void Start(){
		Pause();
		MakeMenu();
	}

	MenuItemList menu;
	void MakeMenu(){
		menu = MenuItemList.Create<MenuItemList>(new Vector3(Screen.width / 2f, Screen.height / 2f));
		menu.AddItem(MenuItem.Create("Unpause", menu_UnpauseGame));
		menu.AddItem(MenuItem.Create("Return to Main Menu", menu_ReturnToHub));

		menu.transform.SetParent(transform, false);

		foreach(var m in menu.menuItemList){
			m.font.Align_H = DrawBitmapFont.Halign.Center;
			m.font.Align_V = DrawBitmapFont.Valign.Middle;
		}
	}

	void menu_UnpauseGame(MenuItem source, int val){
		Unpause();
	}

	void menu_ReturnToHub(MenuItem source, int val){
		G1_GameManager.Instance.LoadNewScene("Shell");
		menu.DestroyMenuChain();
	}
}
