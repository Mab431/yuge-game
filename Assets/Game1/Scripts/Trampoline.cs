﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Trampoline : MonoBehaviour , IPlayerTrigger{

	public Transform BouncePad;

	//animator info
	Animator animator;
	int anim_bounce;

	public bool RelativeBounce = false;
	public float BounceThreshold = 0f;
	//absolute
	public float BouncePower = 10f;
	//relative
	public float BounceScale = 1f;

	void Start(){
		animator = GetComponent<Animator>();
		anim_bounce = Animator.StringToHash("Bounce");
	}

	public void ActivateTrigger(GameObject go){
		G1_PlayerCharacter player = go.GetComponent<G1_PlayerCharacter>();
		if(player != null){
			if(player.VerticalSpeed < BounceThreshold){
				if(RelativeBounce){
					player.Jump(Mathf.Abs(player.VerticalSpeed * BounceScale));
				}else{
					player.Jump(BouncePower);
				}
				Animate();
			}
		}
	}

	public void Animate(){
		animator.Play(anim_bounce);
	}
}
