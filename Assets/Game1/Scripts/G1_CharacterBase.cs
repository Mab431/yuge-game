﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class G1_CharacterBase : MonoBehaviour {

	public enum DamageType {
		OutOfBounds,
		Damage,
		Crushed,
		Drowned,
		Unknown
	}

	public enum CharacterType {
		Player,
		Friendly,
		Enemy,
		Neutral
	}

	public string Name;
	public int HP = 3;
	public int Def = 0;
	public int Pow = 1;
	protected bool _isDead = false;
	public CharacterType CharType;
	public Transform SpawnPoint;
	DamageType _lastDamagedType = DamageType.Unknown;

	protected Vector3 velocity = Vector3.zero;
	protected bool isGrounded;
	protected bool isOnSlope;
	protected bool isAirborn;
	protected bool isUnderWater;
	protected float groundFriction = 20f;
	protected float waterFriction = 5f;

	protected CharacterController control;

	//public vars
	public Transform GroundPosition;
	public Transform WaterPosition;
	public LayerMask GroundLayer;
	public LayerMask WaterLayer;
	public float GravityPower = 9.81f;
	public float GravityCap = 32f;
	public float GroundCheckRadius = 0.25f;
	public float SlopeCheckRadius = 0.5f;
	public float MaxSlopeAngle = 45f;
	public float WaterCheckRadius = 0.5f;
	public float WaterForceScale = 0.25f;

	public float VerticalSpeed {
		get {
			return velocity.y;
		}
	}

	List<G1_MovingPlatform> platforms;

	public delegate void HealthChangeArgs (int hpBefore, int hpNow);

	public HealthChangeArgs OnHealthChanged;

	[SerializeField]
	float FallKillPlane = -32;
	[SerializeField]
	Vector3 groundNormal;

	public virtual void Start () {

		control = GetComponent<CharacterController> ();
	}

	void CheckSlide(){
		Ray groundRay = new Ray (transform.position, Vector3.down);
		RaycastHit hit;
		Physics.Raycast (groundRay, out hit, 5f, GroundLayer);
		groundNormal = hit.normal;
		if (groundNormal.y < (Mathf.Sin(MaxSlopeAngle * Mathf.Deg2Rad))) {
			velocity.y = -GravityPower;
			velocity.x = groundNormal.x * GravityPower;
			velocity.z = groundNormal.z * GravityPower;
		}
	}

	protected void SnapToGround(){
		//if (isOnSlope) {

		float dis = SlopeCheckRadius;
		Ray groundRay = new Ray (transform.position, Vector3.down);
		RaycastHit hit;
		Physics.Raycast (groundRay, out hit, dis, GroundLayer);
		if (hit.distance < dis && velocity.y < 0f) {
			//velocity.y = 0;
			control.Move (Vector3.down * hit.distance);
			//isGrounded = true;
			//print(hit.distance);
		}
		//}
	}

	public virtual void Update () {

		//underwater
		if (isUnderWater) {
			if (velocity.y > -GravityCap * WaterForceScale) {
				velocity.y -= GravityPower * WaterForceScale * Time.deltaTime;
			}
			if (velocity.y < -GravityCap * WaterForceScale) {
				velocity.y += GravityPower * WaterForceScale;
			}
		}

		//frictions
		if (isGrounded) {
			velocity = Vector3.MoveTowards (velocity, new Vector3 (0, velocity.y, 0), Time.deltaTime * groundFriction);
		}

		control.Move (velocity * Time.deltaTime);
		SnapToGround ();

		//check Out of bounds
		if (transform.position.y < FallKillPlane) {
			HP = 0;
			_lastDamagedType = DamageType.OutOfBounds;
			CheckDeath ();
		}

		if (!isGrounded) {
			if (!isUnderWater) {
				if (velocity.y > -GravityCap) {
					velocity.y -= GravityPower * Time.deltaTime;
				}
			}
		}

		if (isOnSlope && !isUnderWater) {
			CheckSlide ();
		}

		//check for ground
		isGrounded = Physics.CheckSphere (GroundPosition.transform.position, GroundCheckRadius, GroundLayer, QueryTriggerInteraction.Ignore);
		isOnSlope = Physics.CheckSphere (GroundPosition.transform.position, SlopeCheckRadius, GroundLayer, QueryTriggerInteraction.Ignore);
		//isGrounded = control.isGrounded;
		//check for water
		isUnderWater = Physics.CheckSphere (WaterPosition.transform.position, GroundCheckRadius, WaterLayer, QueryTriggerInteraction.Collide);
	}

	public void ApplyDamage (int damage, DamageType damageType) {
		if (!_isDead) {
			int change = Mathf.Max (HP - damage, 0);
			int hpBefore = HP;
			HP = change;
			_lastDamagedType = damageType;


			OnHealthChanged (hpBefore, HP);
			CheckDeath ();
		}
	}

	public void ApplyForce (Vector3 force) {
		velocity += force * Time.deltaTime;
	}

	bool CheckDeath () {
		if (HP <= 0 && !_isDead) {
			HP = 0;
			_isDead = true;
			OnDeath (_lastDamagedType);
			print (Name + " is dead!");
			return true;
		}
		return false;
	}

	public bool IsDead () {
		return _isDead;
	}

	protected void GoToSpawn (float timer = 1f) {
		StartCoroutine (co_Respawn (timer));
	}

	protected virtual IEnumerator co_Respawn (float timer) {

		float t = 0;
		//wait
		while (t < timer) {
			t += Time.deltaTime;
			yield return null;
		}
		//return to spawn
		if (SpawnPoint != null) {
			transform.position = SpawnPoint.position;
		}
	}

	public abstract void OnDeath (DamageType deathBy);
}
