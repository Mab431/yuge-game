﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class G1_ObjectSpawner : ButtonTrigger{

	public Vector3 SpawnArea = Vector3.zero;
	public List<GameObject> SpawnObjects = new List<GameObject>();
	public bool SpawnOnStart = false;
	public bool SpawnOnPressed = true;
	public bool SpawnOnRelesed = false;
	/// <summary>
	/// Warning! This has performance implications
	/// </summary>
	public bool SpawnOnHeld = false;
	/// <summary>
	/// Warning! This has performance implications
	/// </summary>
	public bool SpawnOnNotHeld = false;

	// Use this for initialization
	void Start () {
		if(SpawnOnStart){
			Spawn();
		}
	}

	void Spawn(){
		Vector3 area = transform.position + new Vector3(
			Random.Range(-SpawnArea.x, SpawnArea.x) / 2f,
			Random.Range(-SpawnArea.y, SpawnArea.y) / 2f,
			Random.Range(-SpawnArea.z, SpawnArea.z) / 2f);
		int index = Random.Range(0,SpawnObjects.Count-1);

		Instantiate(SpawnObjects[index], area, transform.rotation);
	}

	public override void ButtonAction (GameObject source, G1_PressureButton.ButtonState state) {
		print(state);
		if(SpawnOnPressed && state == G1_PressureButton.ButtonState.Pressed){
			Spawn();
		}
		if(SpawnOnRelesed && state == G1_PressureButton.ButtonState.Released){
			Spawn();
		}
		if(SpawnOnHeld && state == G1_PressureButton.ButtonState.Held){
			Spawn();
		}
		if(SpawnOnNotHeld && state == G1_PressureButton.ButtonState.NotHeld){
			Spawn();
		}
	}
}
