﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class G1_CollectibleSystem : MonoBehaviour
{
	public Texture2D Icon;
    public List<G1_CollectibleItem> collectibleList = new List<G1_CollectibleItem>();
    public List<ButtonTrigger> triggerList = new List<ButtonTrigger>();
	public int Total;
	int _count = 0;
	public int Collected{
		get{
			return _count;
		}
		set{
			if(OnCountChanged != null){
				OnCountChanged(_count, value);
			}
			_count = value;
		}
	}

	public delegate void CountChangedEvent(int prev, int current);
	public CountChangedEvent OnCountChanged;
    
    void Start()
    {
		Total = 0;
		Collected = 0;
        foreach(G1_CollectibleItem c in transform.GetComponentsInChildren<G1_CollectibleItem>())
        {
            collectibleList.Add(c);
            c.collectible = this;
			Total ++;
        }
    }

    public void itemCollected(G1_CollectibleItem c)
    {
		if(Collected == 0){
			G1_HUD.Current.CreateNewSystem(this);
		}

        collectibleList.Remove(c);

		Collected++;

        if(collectibleList.Count == 0)
        {
            foreach(var v in triggerList)
            {
                v.ButtonAction(gameObject, G1_PressureButton.ButtonState.Pressed);
            }
        }
    }
}