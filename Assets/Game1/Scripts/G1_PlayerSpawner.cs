﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class G1_PlayerSpawner : MonoBehaviour {

	public G1_PlayerCharacter PlayerPrefab;
	public Game1Camera CameraPrefab;
	public G1_HUD HUDPrefab;

	// Use this for initialization
	void Start () {
		var player = Instantiate <G1_PlayerCharacter> (PlayerPrefab, transform.position, Quaternion.identity);
		var hud = Instantiate<G1_HUD> (HUDPrefab);
		var cam = Instantiate<Game1Camera> (CameraPrefab, player.transform.position, Quaternion.Euler(new Vector3(25,0,0)));

		player.SpawnPoint = transform;
		cam.target = player.transform;
		hud.PlayerRef = player;
	}
}
