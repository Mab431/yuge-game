﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GamewingResourceLibrary;
using GamewingResourceLibrary.MenuSystem;
using UnityEngine.SceneManagement;

public class Shell_MainMenu : MenuItemList {

	public Transform subMenuPosition;

	public Color colorSelected = new Color(0.75f, 0.75f, 0.25f);
	public Color colorDeselcted = new Color(0.25f, 0.75f, 1f);

	// Use this for initialization
	void Start () {
		AddMenuItems ();
		base.Start ();
		Time.timeScale = 1f;
	}

	void AddMenuItems(){
		transform.localScale = Vector3.one;

		ItemSize = 64f;
		AddItem (MenuItem.Create ("Space Cowboy Adventures", menu_Platformer));
		AddItem (MenuItem.Create ("Trombone Hero", menu_TromboneHero));
		AddItem (MenuItem.Create ("Space Invaders", menu_SpaceInvaders));
		AddItem (MenuItem.Create ("Options", menu_Options));
		AddItem (MenuItem.Create ("Quit", menu_Quit));

		//set colors
		foreach (var m in menuItemList){
			m.colorSelected = colorSelected;
			m.colorDeselected = colorDeselcted;
		}
	}

	void menu_Platformer(MenuItem source, int val){
		G1_GameManager.Instance.InitGame();
		DisposeOfMenu();
	}

	void menu_TromboneHero(MenuItem source, int val){
		G1_GameManager.Instance.LoadNewScene ("starter");
		DisposeOfMenu ();
	}

	void menu_SpaceInvaders(MenuItem source, int val){
		G1_GameManager.Instance.LoadNewScene ("2Dtest_scene");
		DisposeOfMenu ();
	}

	void menu_Options(MenuItem source, int val){
		var m = Menu_Options.Create<Menu_Options>(Vector3.zero);
		SetSubList(m);
		m.transform.position = subMenuPosition.position;
	}

	void menu_Quit(MenuItem source, int val){
		Application.Quit ();
	}
}
