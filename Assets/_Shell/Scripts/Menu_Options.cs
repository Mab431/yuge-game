﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GamewingResourceLibrary.MenuSystem;

public class Menu_Options : MenuItemList {

	void Start(){
		GenerateItems();
	}

	void GenerateItems(){
		for (int i = 0; i < QualitySettings.names.Length; i++){
			string n = QualitySettings.names[i];
			AddItem(MenuItem.Create(n, menu_ChangeQuality, i));
		}

		AddItem(MenuItem.Create("Back", menu_GoBack, 0));
	}

	void menu_GoBack(MenuItem source, int val){
		DisposeOfMenu();
	}

	void menu_ChangeQuality(MenuItem source, int val){
		QualitySettings.SetQualityLevel(val, true);
	}
}
