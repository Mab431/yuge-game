﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shell_Camera : MonoBehaviour {

	public float Speed = 0.5f;
	public float maxDistance = 0.5f;

	Vector3 targetDir = Vector3.zero;
	Vector3 newpos = Vector3.zero;
	float timer = 0f;
	
	// Update is called once per frame
	void Update () {
		Vector3 perlin = new Vector3 (
			                 Mathf.PerlinNoise (timer, 0f),
			                 Mathf.PerlinNoise (0f , timer),
			                 Mathf.PerlinNoise (timer, timer)
		                 );

		transform.localPosition = perlin;

		//targetDir = Vector3.Slerp (targetDir, newpos, Time.deltaTime);

		timer += Time.deltaTime * Speed;
	}

	void ChangePosition(){
		//newpos = Random.insideUnitSphere * maxDistance;
	}
}
