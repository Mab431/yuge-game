﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Timers;
using UnityEngine.SceneManagement;

public class SI_Loading : MonoBehaviour {
    private Timer t = new Timer(200);
    private static string retrySI = "2Dtest_scene";
    public SI_Single s = SI_Single.getInstance();
    // Use this for initialization
    void Start ()
    {
        s.current_enemy = 32;
        if (s.player_health == 0)
        {
            s.player_health += 3;
        }
        SceneManager.LoadScene(retrySI);

    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
