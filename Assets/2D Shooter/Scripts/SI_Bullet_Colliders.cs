﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SI_Bullet_Colliders : MonoBehaviour {

    // Use this for initialization
    public int current_score = 0;
	
	// Update is called once per frame
	void OnTriggerEnter (Collider col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            Destroy(gameObject);
            Destroy(col.gameObject);
        }
        
        else if (col.gameObject.name != "Player")
        {
            Destroy(gameObject);
        }
        
	}



}
