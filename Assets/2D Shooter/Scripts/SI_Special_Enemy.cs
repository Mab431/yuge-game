﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SI_Special_Enemy : MonoBehaviour
{
    private static string retrySI = "SI_Loading";
    private static string quitSI = "Shell";


    void OnTriggerEnter(Collider col)
    {


        if (col.CompareTag("Bullet"))
        {

            Destroy(gameObject);
            Destroy(col.gameObject);
            if (gameObject.tag == "Retry")
            {
                SceneManager.LoadScene(retrySI);
            }


            else if((gameObject.tag == "Quit"))
            {
                SceneManager.LoadScene(quitSI);
            }
            

        }

    }
}
