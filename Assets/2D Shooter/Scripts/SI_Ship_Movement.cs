﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GamewingResourceLibrary;

public class SI_Ship_Movement : MonoBehaviour
{

    protected Vector3 movement = Vector3.zero;
    protected CharacterController controller;
    public AudioClip fire;
    public GameObject bullet;
    private static string quitSI = "Shell";

    SuperInput.ControllerNode input_shoot;
	SuperInput.ControllerNode input_moveX;
    SuperInput.ControllerNode input_quit;

    // Use this for initialization
    void Start ()
    {
        input_quit = GlobalInput.Instance.controller.controls[4];
        input_shoot = GlobalInput.Instance.controller.controls[2];
		input_moveX = GlobalInput.Instance.controller.controls[0];
        controller = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		movement = new Vector3(input_moveX.getValue(), 0 , 0);
        movement = transform.TransformDirection(movement);
        movement *= 8;
        controller.Move(movement * Time.deltaTime);

		if (input_shoot.getPressed())
        {
            //audio.PlayOneShot(fire);
            GetComponent<AudioSource>().Play();
			var spawn_bullet = Instantiate<GameObject>(bullet, transform.position, Quaternion.identity);
            //spawn_bullet.rigidbody.AddForce(Vector3.up * 50);
            spawn_bullet.GetComponent<Rigidbody>().AddForce(Vector3.up * 500);
        }

        if (input_quit.getPressed() || Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene(quitSI);
        }
	}
}
