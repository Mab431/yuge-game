﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SI_Enemy_Bullet : MonoBehaviour {

    public SI_Single s;

    void Start()
    {
        s = SI_Single.getInstance();
    }

    void Update()
    {
        if (transform.position.y < -10.8)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "Player")
        {
            Destroy(gameObject);
            
            s.player_health -= 1;
            if (s.player_health == 0)
            {
                Destroy(col.gameObject);
                print("You Lose!");
            }
        }
        else if (col.gameObject.tag == "Bullet")
        {
            Destroy(gameObject);
            Destroy(col.gameObject);
        }
        else if (col.gameObject.tag != "Enemy")
        {
            Destroy(gameObject);
        }

    }



}
