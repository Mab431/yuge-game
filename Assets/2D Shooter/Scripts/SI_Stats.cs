﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SI_Stats : MonoBehaviour {
    //public GUISkin style;
    public int current_score = 0;
    public int current_enemy = 32;
    public SI_Single s;
    private static string loseGame = "SI_Lose";
    private static string reloadScene = "SI_Loading";
    public GUIStyle style;

    // Use this for initialization


    void Start ()
    {
        s = SI_Single.getInstance();
		s.current_enemy = 32;
    }

	float enemyCheck = 0f;

    void Awake()
    {
        
    }
	// Update is called once per frame
	void Update ()
    {
        
		if (enemyCheck > 2f) {
			if (GameObject.FindObjectsOfType<SI_Enemy> ().Length == 0) {
				SceneManager.LoadScene (reloadScene);
            
			}
			enemyCheck = 0f;
		}

		enemyCheck += Time.deltaTime;

        if (s.player_health == 0)
        {
            SceneManager.LoadScene(loseGame);
        }

    }

    void OnGUI()
    {
        //GUI.skin = style;
        //GUI.Label(new Rect(0,20,200,50 ), "SCORE < " + s.current_score.ToString() + " >" , style); //writes stuff to the screen.
        //GUI.Label(new Rect(0, 35, 200, 50), "LIFE < " + s.player_health.ToString() + " >", style);
    }
}
