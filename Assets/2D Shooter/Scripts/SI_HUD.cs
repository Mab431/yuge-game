﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SI_HUD : MonoBehaviour {

    public SI_Single s;
    public DrawBitmapFont _health, _score;


    void Start ()
    {

        s = SI_Single.getInstance();
        s.onLifeChanged += OnHealthChanged;
        s.onScoreChanged += OnScoreChanged;


        OnHealthChanged(s.player_health);
        OnScoreChanged(s.current_score);


    }

    void OnScoreChanged(int val)
    {
        _score.Text = "SCORE < " + val.ToString() + " >";
    }

    void OnHealthChanged(int val)
    {
        _health.Text = "HEALTH < " + val.ToString() + " >";
    }
}
