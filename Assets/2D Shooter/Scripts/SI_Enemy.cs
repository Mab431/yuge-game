﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SI_Enemy : MonoBehaviour
{

    static bool go_left = false;
    public int speed = 5;
    public SI_Single s;
    private int timer = 300;
    private int timer_two = 300;
    private float down = float.Parse("0.1");
    private bool go_down = false;
    private int initial_enemy = 32;
    public GameObject enemy_bullet;
    

	void Start ()
    {
        //si_stats = GetComponent<SI_Stats>();
        s = SI_Single.getInstance();

    }
	
	// Update is called once per frame
	void Update ()
    {
        var deltaTime = Time.deltaTime;

        SpeedUp();


        if (go_left)
        {
            Vector3 newpos = transform.position;
            newpos.x -= speed * deltaTime;
            transform.position = newpos;
            
        }
        else
        {

            Vector3 newpos = transform.position;
            newpos.x += speed * deltaTime;
            transform.position = newpos;
            

        }
        timer -= 1;
        timer_two -= 1;
        if (timer < 2)
        {
            var rand_num = Random.Range(0, 8);
            var num_chosen = rand_num;

            if (num_chosen == 2)
            {
                var spawn_bullet = Instantiate<GameObject>(enemy_bullet, transform.position, Quaternion.identity);
                spawn_bullet.GetComponent<Rigidbody>().AddForce(Vector3.down * 500);

            }
            timer = 300;

        }


        //MoveDown();


	}

	void OnTriggerEnter(Collider col)
    {
		if(col.CompareTag("Bullet"))
        {
            
			if (s == null) {
				s = SI_Single.getInstance ();
			}

			Destroy(gameObject);
            Destroy(col.gameObject);
            if (gameObject.name.Contains("02"))
            {
                s.current_score += 50;
            }
            else
            {
                s.current_score += 25;
            }
            
            s.current_enemy -= 1;
            
        }

        if (col.gameObject.name =="Border_Left")
        {

            go_left = false;
			MoveDown();
        }

        if (col.gameObject.name == "Border_Right")
        {

            go_left = true;
			MoveDown();
        }

        if (col.gameObject.name == "Player")
        {
            s.player_health = 0;
        }


	}


    void MoveDown()
    {
        var enemies = GameObject.FindGameObjectsWithTag("Enemy");

        foreach(var i in enemies)
        {
            Vector3 newposY = i.transform.position;
            newposY.y -= down;
            if (s.current_enemy == 25)
            {
                newposY.y -= 2 * down;
            }
            if (s.current_enemy <= initial_enemy / 2)
            {
                newposY.y -= 4 * down;
            }
            
            if (s.current_enemy == 1)
            {   
                newposY.y -= 20 * down;
            }
            if (newposY.y < -10.53)
            {
                newposY.y = (float)-10.53;

            }
                i.transform.position = newposY;
        }
    }

    void SpeedUp()
    {
        if (s.current_enemy == 25)
        {
            speed = 6;
        }
        if (s.current_enemy <= initial_enemy / 2)
        {
            speed = 8;
        }
        if (s.current_enemy <= initial_enemy / 4)
        {
            speed = 12;
        }
        if (s.current_enemy == 1)
        {
            speed = 20;
        }
    }
    

}
