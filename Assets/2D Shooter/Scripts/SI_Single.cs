﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SI_Single
{

	private static SI_Single instance;
    public int current_enemy;
    public int current_score
    {
        get { return _score; }
        set
        {
            if(onScoreChanged != null)
                onScoreChanged(value);
            _score = value;
        }


    }

    public int player_health
    {
        get { return _health; }
        set
        {
            if (onLifeChanged != null)
                onLifeChanged(value);
            _health = value;
        }


    }
    private int _health;
    private int _score;
    public delegate void scoreChangedEvent(int score);
    public delegate void liveChangedEvent(int lives);



    public scoreChangedEvent onScoreChanged;
    public liveChangedEvent onLifeChanged;


    private SI_Single()
    {
        current_score = 0;
        player_health = 3;
        current_enemy = 32;//change as you add more enemies.
    }

    public static SI_Single getInstance()
    {
        if (instance == null)
        {
            instance = new SI_Single();
        }
        return instance;
    }

}
