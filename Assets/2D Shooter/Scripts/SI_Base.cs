﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SI_Base : MonoBehaviour {

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Bullet")
        {
            Destroy(gameObject);
            Destroy(col.gameObject);
        }
        else if (col.gameObject.tag == "Enemy_Bullet")
        {
            Destroy(gameObject);
            Destroy(col.gameObject);
        }
        else if (col.gameObject.tag == "Enemy")
        {
            Destroy(gameObject);
        }

    }
}
