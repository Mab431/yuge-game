﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class EnemyMovement2 {

	[Test]
	public void EnemyMovement2SimplePasses()
    {
		// Use the Assert class to test conditions.
	}

	// A UnityTest behaves like a coroutine in PlayMode
	// and allows you to yield null to skip a frame in EditMode
	[UnityTest]
	public IEnumerator EnemyMovement2WithEnumeratorPasses()
    {
        // Use the Assert class to test conditions.
        // yield to skip a frame
        var enemy = new GameObject().AddComponent<SI_Enemy>();


        Assert.AreEqual(enemy.speed,5);
		yield return null;
	}
}
