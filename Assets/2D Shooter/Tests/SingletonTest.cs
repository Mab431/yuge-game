﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class SingletonTest {

	[Test]
	public void SingletonTestSimplePasses() {
		// Use the Assert class to test conditions.
	}

	// A UnityTest behaves like a coroutine in PlayMode
	// and allows you to yield null to skip a frame in EditMode
	[UnityTest]
	public IEnumerator SingletonTestWithEnumeratorPasses() {
        // Use the Assert class to test conditions.
        // yield to skip a frame
        var enemy = new GameObject().AddComponent<SI_Enemy>();

        Assert.IsNull(enemy.s);
		yield return null;
	}
}
