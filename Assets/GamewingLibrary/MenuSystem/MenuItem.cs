﻿using System;
using UnityEngine;

namespace GamewingResourceLibrary.MenuSystem {
	public class MenuItem: MonoBehaviour{
		private MenuItem () {
			
		}

		//members
		public bool enabled;
		public bool selected = false;
		public Color colorSelected = Color.green;
		public Color colorDeselected = Color.red;
		public Color colorDisabled = Color.gray;
		public Color colorSelectedNotFocused = Color.yellow;
		Color currentColor = Color.red;
		public string itemName;
		public string itemDescription;
		public int value;
		float _size;
		public float Size{
			get{ 
				return _size;
			}
			set{ 
				_size = value;
				font.stringSize = value;
			}
		}

		public DrawBitmapFont font;

		//events
		public delegate void menuEventType (MenuItem source, int val);
		menuEventType itemAction = null;
		menuEventType itemNegativeAction = null;
		menuEventType itemSelected = null;
		menuEventType itemDeselected = null;
		menuEventType itemFocusLost = null;
		menuEventType itemFocusGained = null;

		public static MenuItem Create(string name, menuEventType Action, int val = 0){
			GameObject go = new GameObject(name);
			MenuItem itemRef = go.AddComponent<MenuItem>();
			itemRef.font = go.AddComponent<DrawBitmapFont> ();

			itemRef.itemAction = Action;
			itemRef.itemName = name;
			itemRef.enabled = true;

			itemRef.value = val;

			itemRef.font.Text = name;

			itemRef.Deselected ();

			return itemRef;
		}

		public void SetPosition(Vector3 pos){
			transform.localPosition = pos;
		}

		public void Action () {
			if(itemAction != null){
				itemAction.Invoke(this, value);
			}
		}
		public void NegativeAction(){
			if(itemNegativeAction != null){
				itemNegativeAction.Invoke(this, value);
			}
		}
		public void Selected(){
			selected = true;
			currentColor = colorSelected;
			font.StringColor = currentColor;
			if(itemSelected != null){
				itemSelected.Invoke(this, value);
			}
		}
		public void Deselected(){
			selected = false;
			currentColor = colorDeselected;
			font.StringColor = currentColor;
			if(itemDeselected != null){
				itemDeselected.Invoke(this, value);
			}
		}
		public void FocusLost(){
			if (selected) {
				currentColor = colorSelectedNotFocused;
				font.StringColor = currentColor;
			} else {
				currentColor = colorDisabled;
				font.StringColor = currentColor;
			}
			if (itemFocusLost != null) {
				itemFocusLost.Invoke (this, 0);
			}
		}
		public void FocusGained(){
			if (selected) {
				currentColor = colorSelected;
				font.StringColor = currentColor;
			} else {
				currentColor = colorDeselected;
				font.StringColor = currentColor;
			}
			if (itemFocusGained != null) {
				itemFocusGained.Invoke (this, 0);
			}
		}

		public void DrawItem (Rect rec) {
			/*
			Vector3 pos = transform.position;
			Color colorPrev = GUI.color;
			GUI.color = currentColor;

			float UIScale = Screen.height / 1080f;

			//GlobalVars.Instance.font.DrawFont(new Vector2(pos.x * UIScale,pos.y * UIScale), 24 * UIScale, gameObject.name);
			GUI.color = colorPrev;
			*/
		}
	}


}

