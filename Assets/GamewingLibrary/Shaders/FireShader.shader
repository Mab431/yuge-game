﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "FX/Fire"
{
	Properties
	{
		[HDR] _Color ("Main Color", Color) = (1,1,1,1)
		[HDR] _Color2 ("Secondary Color", Color) = (0.5,0.5,0.5,0.5)
		_MainTex ("Texture", 2D) = "white" {}
		_MaskTex ("Clipping Mask", 2D) = "White" {}
		_Cutoff ("Alpha Cutoff", range(0.0,1.0)) = 0.5
		_HSpeed ("Animation Horizontal Speed", float) = 16
		_VSpeed ("Animation Verticle Speed", float) = 0
		_AlphaOffset ("Cutoff Offset", range(0.0,1.0)) = 0
	}
	SubShader
	{
		Tags { "RenderType"="Transparent" "Queue"="Transparent" "IgnoreProjector"="true"}
		Blend SrcAlpha OneMinusSrcAlpha
		LOD 100
		cull off

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 uv : TEXCOORD0;
				float2 uvMask : TEXCOORD1;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				//float2 uvMask : TEXCOORD1;
				fixed4 color : COLOR;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			float4 _Color;
			float4 _Color2;
			sampler2D _MainTex;
			sampler2D _MaskTex;
			float4 _MainTex_ST;
			float _Cutoff;
			float _HSpeed;
			float _VSpeed;
			float _AlphaOffset;
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.color = v.color;
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 mask = tex2D(_MaskTex, i.uv - float2(_Time.x * _HSpeed,_Time.x * _VSpeed));
				float alpha = i.color.a * col.a * mask.a + _AlphaOffset;
				clip(alpha - _Cutoff);
				col.a = 1;
				col *= lerp(_Color2, _Color, (alpha-_Cutoff)/(1-_Cutoff));
				col *= i.color;
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
