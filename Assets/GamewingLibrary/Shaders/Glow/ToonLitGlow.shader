Shader "Toon/Glow/Lit" {
	Properties {
		_Color ("Main Color", Color) = (0.5,0.5,0.5,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Ramp ("Toon Ramp (RGB)", 2D) = "gray" {}
		_GlowTex ("Glow Texture", 2D) = "black" {}
		_GlowIntensity ("Glow Intensity", float) = 0.0
	}

	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
CGPROGRAM
#pragma surface surf ToonRamp


sampler2D _Ramp;
sampler2D _GlowTex;
int _UseGlowTex;
float _GlowIntensity;

// custom lighting function that uses a texture ramp based
// on angle between light direction and normal
#pragma lighting ToonRamp exclude_path:prepass
inline half4 LightingToonRamp (inout SurfaceOutput s, half3 lightDir, half atten)
{
	#ifndef USING_DIRECTIONAL_LIGHT
	lightDir = normalize(lightDir);
	#endif
	
	half d = dot (s.Normal, lightDir)*0.5 + 0.5;
	half3 ramp = tex2D (_Ramp, float2(d,d)).rgb  * (atten * 2);
	half light = ramp.r;

	half4 c;
	c.rgb = s.Albedo.rgb * _LightColor0.rgb * ramp.rgb;
	c.a = s.Alpha;
	return c;
}


sampler2D _MainTex;
float4 _Color;

struct Input {
	float2 uv_MainTex : TEXCOORD0;
	float2 uv_GlowTex : TEXCOORD1;
};

void surf (Input IN, inout SurfaceOutput o) {
	half4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
	half3 g = tex2D(_GlowTex, IN.uv_GlowTex).rgb;
	o.Albedo = c.rgb;
	o.Emission = g.rgb * _GlowIntensity;
	o.Alpha = c.a;
}
ENDCG

	} 

	Fallback "Diffuse"
}
