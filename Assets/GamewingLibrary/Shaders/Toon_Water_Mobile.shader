﻿Shader "Toon/Water Mobile" {
	Properties {
		_Color("Main Color", Color) = (0.5, 0.5, 0.5, 1)
		_Color2("Secondary Color", Color) = (0.5, 0.5, 0.5, 0.5)
		_MainTex("Base (RGB)", 2D) = "white" {}
		_MaskTex("Clipping Mask", 2D) = "White" {}
		_Height("Displacement Map", 2D) = "Gray" {}
		_Normal("Normal Map", 2D) = "bump" {}
		_NrmScale("Normal Intensity", float) = 1
		_Ramp("Toon Ramp (RGB)", 2D) = "gray" {}
		_Anim1_x("Main Texture X Animation", float) = 0
		_Anim1_y("Main Texture Y Animation", float) = 0
		_Anim2_x("Secondary Texture X Animation", float) = 0
		_Anim2_y("Secondary Texture Y Animation", float) = 0
		_Anim3_x("Offset Texture X Animation", float) = 0
		_Anim3_y("Offset Texture Y Animation", float) = 0

		//_Cutoff ("Alpha Cutoff", Range(0.0,1.0)) = 0.5
			[KeywordEnum(Off, Blend, Multiply)] _ReflMode("Reflection Mode", int) = 0
		_Cube("Reflection", Cube) = "_SkyBox" {}
		_ReflIntensity("Reflection Intensity", range(0.0, 1.0)) = 0.5
		_Displacement("Texture Displacement", float) = 1
		_DispOffset("Displacement Offset", float) = 0
		_Tess("Tesselation", Range(1, 32)) = 4
		_refractionAmount("Refraction Amount", Range(-1.0, 1.0)) = 0.1
		_refractionBlend("Refraction Blend", Range(0.0, 1.0)) = 0.5
	}

	SubShader {
		GrabPass {
			Name "BASE"
			Tags {
				"LightMode" = "Always"
			}
		}

		Pass {
			Tags {
				"LightMode" = "ShadowCaster"
			}
			Cull Off

			CGPROGRAM
			#pragma multi_compile_shadowcaster
			//#include "UnityCG.cginc"

			struct v2f {
				V2F_SHADOW_CASTER;
			};

			v2f vert(appdata_base v) {
				v2f o;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET(o)
				return o;
			}

			float4 frag(v2f i): SV_Target {
				SHADOW_CASTER_FRAGMENT(i)
			}
			ENDCG
		}


		Tags {
			"RenderType" = "Transparent"
			"Queue" = "AlphaTest"
		}
		Blend SrcAlpha OneMinusSrcAlpha
		Zwrite off //on
		cull back //off
		LOD 200

		CGPROGRAM
		#pragma surface surf ToonRamp vertex:disp keepalpha fullforwardshadows
			//#pragma surface surf Lambert

		struct appdata {
			float4 vertex: POSITION;
			float4 tangent: TANGENT;
			float3 normal: NORMAL;
			float2 texcoord: TEXCOORD0;
			float2 texcoord1: TEXCOORD1;
			float2 texcoord3: TEXCOORD3;
		};

		struct Input {
			float2 uv_MainTex: TEXCOORD0;
			float2 uv_MaskTex: TEXCOORD1;
			float2 uv_Normal: TEXCOORD2;
			float2 uv_Height: TEXCOORD3;
			float4 screenPos;
			//float2 uv_CutoffTex : TEXCOORD3;
			float3 worldRefl;
			INTERNAL_DATA
		};

		// custom lighting function that uses a texture ramp based
		// on angle between light direction and normal

		sampler2D _Ramp;
		float _Cutoff;
		sampler2D _GlowTex;
		int _UseGlowTex;
		float _GlowIntensity;
		int _ReflMode;
		float _ReflIntensity;
		sampler2D _MainTex;
		sampler2D _MaskTex;
		sampler2D _Height;
		//sampler2D _CutoffTex;
		sampler2D _Normal;
		float _NrmScale;
		float4 _Color;
		float4 _Color2;
		float _Anim1_x;
		float _Anim1_y;
		float _Anim2_x;
		float _Anim2_y;
		float _Anim3_x;
		float _Anim3_y;
		samplerCUBE _Cube;
		float _Displacement;
		float _DispOffset;
		float _Tess;
		sampler2D _GrabTexture;
		float _refractionAmount;
		float _refractionBlend;

		#pragma lighting ToonRamp exclude_path: prepass
		inline half4 LightingToonRamp(SurfaceOutput s, half3 lightDir, half atten) {
			#ifndef USING_DIRECTIONAL_LIGHT
			lightDir = normalize(lightDir);
			#endif

			half d = dot(s.Normal, lightDir) * 0.5 + 0.5;
			half3 ramp = tex2D(_Ramp, float2(d, d)).rgb;

			half4 c;
			c.rgb = s.Albedo * _LightColor0.rgb * ramp * (atten * 2);
			c.a = s.Alpha;
			//clip(s.Alpha - _Cutoff);
			return c;
		}

		void disp(inout appdata v) {
			float d = tex2Dlod(_Height, float4(v.texcoord3.xy - float2(_Anim3_x * _Time.x, _Anim3_y * _Time.x), 0, 0)).a * _Displacement + _DispOffset;
			v.vertex.xyz += v.normal * d;
		}

		void surf(Input IN, inout SurfaceOutput o) {
			half4 c;
			half4 p = tex2D(_MainTex, IN.uv_MainTex - float2(_Anim1_x * _Time.x, _Anim1_y * _Time.x));
			half4 m = tex2D(_MainTex, IN.uv_MainTex - float2(_Anim2_x * _Time.x, _Anim2_y * _Time.x));
			float alpha = p.a * m.a;
			c.a = 1;
			c = lerp(_Color2, _Color, alpha);
			//half cut = tex2D(_CutoffTex, IN.uv_CutoffTex).a;
			//half3 g = tex2D(_GlowTex, IN.uv_GlowTex).rgb;
			o.Normal = normalize(lerp(float4(0.5, 0.5, 1.0, 1.0), UnpackNormal(tex2D(_Normal, IN.uv_Normal - float2(_Anim1_x * _Time.x, _Anim1_y * _Time.x))), _NrmScale));

			float3 worldRefl = WorldReflectionVector(IN, o.Normal);
			fixed4 reflcol = texCUBE(_Cube, worldRefl);

			//refract
			c.rgb = lerp(
				c.rgb,
				tex2D(_GrabTexture, (IN.screenPos.xy / IN.screenPos.w) + o.Normal.xy * _refractionAmount),
				_refractionBlend);
			//clip(min(c.a,cut) - _Cutoff);
			//o.Emission = g.rgb * _GlowIntensity;
			if (_ReflMode == 0) {
				o.Albedo = c.rgb;
			} else if (_ReflMode == 1) { //blend
				o.Albedo = lerp(c.rgb, reflcol.rgb, _ReflIntensity * reflcol.a * _Color.rgb);
			} else if (_ReflMode == 2) { //multiply
				o.Albedo = c.rgb * lerp(float3(1, 1, 1), reflcol.rgb, _ReflIntensity);
			}
			//o.Albedo.rgb = float3(c.a,0,0);
			o.Alpha = c.a;
		}
		ENDCG

	}

	Fallback "Transparent/Diffuse"
}