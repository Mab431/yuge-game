Shader "Toon/Advanced_Outline" {
	Properties {
     	[KeywordEnum (Off, Front, Back)] _CullMode ("Cull", int) = 2
		_OutlineColor ("Outline Color", Color) = (0,0,0,1)
		_Outline ("Outline width", Range (.002, 0.03)) = 0.0025
		_Color ("Main Color", Color) = (0.5,0.5,0.5,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Normal ("Normal Map", 2D) = "bump" {}
		_Ramp ("Toon Ramp (RGB)", 2D) = "gray" {}
		_CutoffTex ("Cutoff Image (A)", 2D) = "white" {}
		_Cutoff ("Alpha Cutoff", Range(0.0,1.0)) = 0.5
		_GlowTex ("Glow Texture", 2D) = "white" {}
		_GlowIntensity ("Glow Intensity", float) = 0.0
		[KeywordEnum (Off, Blend, Multiply, ProbeBlend, ProbeMultiply)] _ReflMode ("Reflection Mode", int) = 0
		_Cube ("Reflection", Cube) = "_SkyBox" {}
		_ReflIntensity ("Reflection Intensity", range(0.0,1.0)) = 0.5
		_ReflMask ("Reflection Mask", 2D) = "white" {}
	}

	SubShader {
		Tags {"RenderType"="Transparent"}

		UsePass "Toon/Advanced/FORWARD"
		UsePass "Toon/Basic Outline/OUTLINE"
	}

	Fallback "Transparent/Cutout/Diffuse"
}