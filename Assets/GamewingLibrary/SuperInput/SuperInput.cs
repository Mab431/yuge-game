﻿//Super Input Class
//Mark Bambino, 2017
//Gamewing Studios
//Version 1.2

#region Changelogs
/* CHANGELOG 1.1
 * Added Negative Key Values
 * Set Xinput to only compile on Windows
 *
*/

/* CHANGELOG 1.2
 * Added Clip Ranges
 * Added Remapping Ranges
 * Added Mouse Axis Input
   - "Mouse_X", "Mouse_Y", and "Mouse_Wheel"
     required in Unity Input Manager
*/
#endregion

//RECOMMENDED: Replace Unity input manager file with the included one
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

//compile Xinput on Windows only
#if (UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN)
using XInputDotNetPure;

//using Newtonsoft.Json;
#endif

namespace GamewingResourceLibrary
{
	public class SuperInput
	{

		[Serializable]
		public class ControllerNode
		{
			string inputName { get; set; }

			InputNode input { get; set; }

			public ControllerNode (string Name)
			{
				inputName = Name;
				input = new inTypeUnassigned ();
			}

			public ControllerNode (string Name, InputNode inType)
			{
				inputName = Name;
				input = inType;
			}

			public string getName ()
			{
				return inputName;
			}

			public InputNode getInputNode ()
			{
				return input;
			}

			public void setInputNode (InputNode newNode)
			{
				input = newNode;
			}

			public bool getPressedPositive ()
			{
				return input.getPressedPositive ();
			}

			public bool getPressedNegative ()
			{
				return input.getPressedNegative ();
			}

			public float getValue ()
			{
				return input.getValue ();
			}

			public float getValuePrevious ()
			{
				return input.getValuePrev ();
			}

			public bool getPressed ()
			{
				return input.getPressed ();
			}

			public bool getHeld ()
			{
				return input.getHeld ();
			}

			public bool getReleased ()
			{
				return input.getReleased ();
			}

			public void update ()
			{
				input.update ();
			}
		}

		[Serializable]
		public abstract class InputNode
		{
			protected float value = 0f;
			protected float valuePrev = 0f;
			protected bool isHeld = false;
			protected bool isHeldPrev = false;
			protected float deadzone = 0.2f;
			protected Vector2 clipRange = new Vector2 (-1f, 1f);
			protected Vector2 mapRange = new Vector2 (-1f, 1f);
			protected bool invert;

			public void SetValue(float val, bool silent = false){
				value = val;
				if (!silent) {
					Debug.Log ("You are directly setting an input value");
				}
			}

			public float getValue ()
			{
				float temp;
				temp = getCalibratedValue (value, -1f, 1f, mapRange.x, mapRange.y);
				temp = Mathf.Clamp (temp, clipRange.x, clipRange.y);
				if (invert) {
					temp = -temp;
				}
				if (Mathf.Abs (temp) < deadzone) {
					temp = 0;
				}
				return temp;
			}

			public float getValuePrev ()
			{
				float temp;
				temp = getCalibratedValue (valuePrev, -1f, 1f, mapRange.x, mapRange.y);
				temp = Mathf.Clamp (temp, clipRange.x, clipRange.y);
				if (invert) {
					temp = -temp;
				}
				return temp;
			}

			public bool getPressedPositive ()
			{
				if (getPressed () && getValue () > 0) {
					return true;
				}
				return false;
			}

			public bool getPressedNegative ()
			{
				if (getPressed () && getValue () < 0) {
					return true;
				}
				return false;
			}

			public bool getPressed ()
			{
				return isHeld && !isHeldPrev;
			}

			public bool getHeld ()
			{
				return isHeld;
			}

			public bool getReleased ()
			{
				return !isHeld && isHeldPrev;
			}

			public void setDeadzone (float dz)
			{
				deadzone = dz;
			}

			public float getDeadzone ()
			{
				return deadzone;
			}

			public void setClipRange (Vector2 range)
			{
				clipRange = range;
			}

			public Vector2 getClipRange ()
			{
				return clipRange;
			}

			public void setMapRange (Vector2 range)
			{
				mapRange = range;
			}

			public Vector2 getMapRange ()
			{
				return mapRange;
			}

			public void setInvert (bool inv)
			{
				invert = inv;
			}

			public bool getInvert ()
			{
				return invert;
			}

			public virtual bool isAssigned ()
			{
				return true;
			}

			public abstract void updateInput ();

			public void update ()
			{
				isHeldPrev = isHeld;
				valuePrev = value;

				updateInput ();

				isHeld = Mathf.Abs (getValue ()) > deadzone;
			}
		}

		public interface IForceFeedback
		{
			void setName (string n);

			string getName ();

			void set (float v, float t);

			void setValue (float v);

			void setTime (float t);

			void stop ();

			float getTime ();

			float getValue ();

			void update ();
		}

		public static float getCalibratedValue (float value, float from1, float to1, float from2, float to2)
		{
			return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
		}

		//Unassigned input class
		public class inTypeUnassigned: InputNode
		{
			public override bool isAssigned ()
			{//overridden so that this returns not assigned
				return false;
			}

			public override void updateInput ()
			{
				value = 0f;
			}
		}

		public class inTypeDriven: InputNode{
			public override bool isAssigned ()
			{
				return true;
			}
			public override void updateInput ()
			{
				//Nothing happens, this value is driven
			}
		}

		//multi input
		[Serializable]
		public class InTypeMulti: InputNode
		{
			List<InputNode> inputList;

			public InTypeMulti () {
				inputList = new List<InputNode>();
			}

			public InTypeMulti (List<InputNode> list) {
				inputList = list;
			}

			public override void updateInput () {
				float posVal = 0;
				float negVal = 0;
				foreach(var i in inputList){
					i.updateInput();
					if(i.getValue() > posVal){
						posVal = i.getValue();
					}
					if(i.getValue() < negVal){
						negVal = i.getValue();
					}
				}
				value = posVal + negVal;
			}
		}

		//keyboard, mouse, and joystick input
		[Serializable]
		public class inTypeKey: InputNode
		{
			KeyCode keyPos;
			KeyCode keyNeg;
			//constructor
			public inTypeKey (KeyCode k)
			{
				keyPos = k;
				keyNeg = KeyCode.None;
			}

			public inTypeKey (KeyCode pos, KeyCode neg)
			{
				keyPos = pos;
				keyNeg = neg;
			}
			//methods
			public override void updateInput ()
			{
				if (Input.GetKey (keyPos)) {
					value = 1.0f;
				} else if (Input.GetKey (keyNeg)) {
					value = -1.0f;
				} else {
					value = 0f;
				}
			}
		}

		[Serializable]
		public class inTypeMouseAxis: InputNode
		{
			public enum MouseAxis
			{
X,
				Y,
				Wheel

			}

			MouseAxis mAxis = MouseAxis.X;
			//constructor
			public inTypeMouseAxis (MouseAxis axis)
			{
				mAxis = axis;
			}

			public override void updateInput ()
			{
				switch (mAxis) {
				case MouseAxis.X:
					value = Input.GetAxisRaw ("Mouse_X");
					break;
				case MouseAxis.Y:
					value = Input.GetAxisRaw ("Mouse_Y");
					break;
				case MouseAxis.Wheel:
					value = Input.GetAxisRaw ("Mouse_Wheel");
					break;
				}
			}
		}

		[SerializeField]
		public class inTypeAxis: InputNode
		{
			string axisName;
			//constructor
			public inTypeAxis (string axisname, float dz = 0.5f)
			{
				axisName = axisname;
				deadzone = dz;
			}

			public override void updateInput ()
			{
				value = Input.GetAxisRaw (axisName);
			}
		}

		#if (UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN)//Xinput
		//xbox 360 inputs
		[Serializable]
		public class inTypeX360Axis: InputNode
		{
//not done

			PlayerIndex player = PlayerIndex.One;

			public enum axisType
			{
LeftX,
				LeftY,
				RightX,
				RightY,
				LeftTrigger,
				RightTrigger

			}

			axisType axis = axisType.LeftX;
			GamePadState state;
			//constructor
			public inTypeX360Axis (PlayerIndex playernum, axisType joystickAxis, float dz = 0.5f)
			{
				player = playernum;
				axis = joystickAxis;
				deadzone = dz;
			}

			public override void updateInput ()
			{
				state = GamePad.GetState (player);
				switch (axis) {
				case axisType.LeftX:
					value = state.ThumbSticks.Left.X;
					break;
				case axisType.LeftY:
					value = state.ThumbSticks.Left.Y;
					break;
				case axisType.RightX:
					value = state.ThumbSticks.Right.X;
					break;
				case axisType.RightY:
					value = state.ThumbSticks.Right.Y;
					break;
				case axisType.LeftTrigger:
					value = state.Triggers.Left;
					break;
				case axisType.RightTrigger:
					value = state.Triggers.Right;
					break;
				}
			}
		}

		[Serializable]
		public class inTypeX360Button: InputNode
		{

			PlayerIndex player = PlayerIndex.One;

			public enum buttonType
			{
A,
				B,
				X,
				Y,
				LB,
				RB,
				Back,
				Start,
				LS,
				RS,
				DPadUp,
				DPadDown,
				DPadLeft,
				DPadRight

			}

			buttonType axis = buttonType.A;
			GamePadState state;
			//constructor
			public inTypeX360Button (PlayerIndex playernum, buttonType b, float dz = 0.5f)
			{
				player = playernum;
				axis = b;
				deadzone = dz;
			}
			//methods

			public override void updateInput ()
			{
				state = GamePad.GetState (player);

				bool btn = false;
				switch (axis) {
				case buttonType.A:
					btn = state.Buttons.A == ButtonState.Pressed;
					break;
				case buttonType.B:
					btn = state.Buttons.B == ButtonState.Pressed;
					break;
				case buttonType.X:
					btn = state.Buttons.X == ButtonState.Pressed;
					break;
				case buttonType.Y:
					btn = state.Buttons.Y == ButtonState.Pressed;
					break;
				case buttonType.LB:
					btn = state.Buttons.LeftShoulder == ButtonState.Pressed;
					break;
				case buttonType.RB:
					btn = state.Buttons.RightShoulder == ButtonState.Pressed;
					break;
				case buttonType.Back:
					btn = state.Buttons.Back == ButtonState.Pressed;
					break;
				case buttonType.Start:
					btn = state.Buttons.Start == ButtonState.Pressed;
					break;
				case buttonType.LS:
					btn = state.Buttons.LeftStick == ButtonState.Pressed;
					break;
				case buttonType.RS:
					btn = state.Buttons.RightStick == ButtonState.Pressed;
					break;
				case buttonType.DPadUp:
					btn = state.DPad.Up == ButtonState.Pressed;
					break;
				case buttonType.DPadDown:
					btn = state.DPad.Down == ButtonState.Pressed;
					break;
				case buttonType.DPadLeft:
					btn = state.DPad.Left == ButtonState.Pressed;
					break;
				case buttonType.DPadRight:
					btn = state.DPad.Right == ButtonState.Pressed;
					break;
				}

				if (btn) {
					value = 1.0f;
				} else {
					value = 0.0f;
				}
			}
		}

		[Serializable]
		public class feedbackTypeX360Rumble : IForceFeedback
		{
			string name;
			PlayerIndex player;

			public enum motorType
			{
Left,
				Right,
				Both

			}

			public motorType motor;
			float value;
			float time;

			public feedbackTypeX360Rumble (string n, PlayerIndex p, motorType m)
			{
				name = n;
				player = p;
				motor = m;
			}

			public void setName (string n)
			{
				name = n;
			}

			public string getName ()
			{
				return name;
			}

			public void set (float v, float t)
			{
				value = v;
				time = t;
			}

			public void setValue (float v)
			{
				value = v;
			}

			public float getValue ()
			{
				return value;
			}

			public void setTime (float t)
			{
				time = t;
			}

			public float getTime ()
			{
				return time;
			}

			public void stop ()
			{
				value = 0.0f;
				time = 0.0f;
			}

			public void update ()
			{
				switch (motor) {
				case motorType.Both:
					if (time > 0) {
						GamePad.SetVibration (player, value, value);
						time -= Time.deltaTime;
					} else {
						GamePad.SetVibration (player, 0.0f, 0.0f);
					}
					break;
				}
			}

		}
		#endif

		//Controller Class
		[Serializable]
		public class ControllerType
		{
			public List<ControllerNode> controls;
			public List<IForceFeedback> feedbackList;

			public ControllerType ()
			{
				controls = new List<ControllerNode> ();
				feedbackList = new List<IForceFeedback> ();
			}

			//easy return functions
			public bool getPressedPositive (int id)
			{
				return controls [id].getPressedPositive ();
			}

			public bool getPressedNegative (int id)
			{
				return controls [id].getPressedNegative ();
			}

			public bool getPressed (int id)
			{
				return controls [id].getPressed ();
			}

			public bool getHeld (int id)
			{
				return controls [id].getHeld ();
			}

			public bool getReleased (int id)
			{
				return controls [id].getReleased ();
			}

			public float getValue (int id)
			{
				return controls [id].getValue ();
			}

			public int getIndex (string name)
			{
				for (int i = 0; i < controls.Count; i++) {
					if (name == controls [i].getName ()) {
						return i;
					}
				}
				return -1;
			}

			public ControllerNode getByName (string name)
			{
				foreach (ControllerNode i in controls) {
					if (name == i.getName ()) {
						return i;
					}
				}
				return null;
			}

			public void update ()
			{
				foreach (ControllerNode i in controls) {
					i.update ();
				}
				foreach (IForceFeedback f in feedbackList) {
					f.update ();
				}
			}

			public void SaveControllerScheme (ControllerType controller, string filePath)
			{
				//string json = JsonConvert.SerializeObject (controller);
				string json = "none";
				Debug.Log (json);
				StreamWriter sw = new StreamWriter (filePath);
				sw.Write (json);
				sw.Close ();
			}

			public ControllerType LoadControllerScheme (string filePath)
			{
				StreamReader sr = new StreamReader (filePath);
				string json = sr.ReadToEnd ();
				sr.Close ();
				//return JsonConvert.DeserializeObject<ControllerType> (json);
				return null;
			}
		}
	}
}