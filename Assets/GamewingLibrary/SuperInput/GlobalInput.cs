﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamewingResourceLibrary {

	public class GlobalInput : MonoBehaviour {

		public SuperInput.ControllerType controller;
		static GlobalInput inst;

		bool debug = false;

		public static GlobalInput Instance {
			get {
				if (inst == null) {
					GameObject go = new GameObject ("Global Input");
					DontDestroyOnLoad (go);
					inst = go.AddComponent<GlobalInput> ();
					inst.InitControls ();
				}
				return inst;
			}
		}

		void OnGUI () {
			if (debug) {
				Vector3 pos = new Vector3 (32, 128, 0);
				foreach (SuperInput.ControllerNode c in controller.controls) {
					GUI.Label (new Rect (pos.x, pos.y, 400, 20),
						c.getName () + ", " + c.getValue ().ToString () + ", "
						+ c.getHeld ().ToString ());
					pos += Vector3.up * 20;
				}
			}
		}

		void InitControls () {
			controller = new SuperInput.ControllerType ();
			controller.controls.Add (new SuperInput.ControllerNode ("P1_Move_X", new SuperInput.InTypeMulti (//0
				new List<SuperInput.InputNode> {
					new SuperInput.inTypeAxis ("P1_Move_X"),
					new SuperInput.inTypeKey (KeyCode.D, KeyCode.A),
					new SuperInput.inTypeKey (KeyCode.RightArrow, KeyCode.LeftArrow)
				})));
			controller.controls.Add (new SuperInput.ControllerNode ("P1_Move_Y", new SuperInput.InTypeMulti (//1
				new List<SuperInput.InputNode> {
					new SuperInput.inTypeAxis ("P1_Move_Y"),
					new SuperInput.inTypeKey (KeyCode.S, KeyCode.W),
					new SuperInput.inTypeKey (KeyCode.DownArrow, KeyCode.UpArrow)
				})));
			controller.controls.Add (new SuperInput.ControllerNode ("Button1", new SuperInput.InTypeMulti (//2
				new List<SuperInput.InputNode> {
					new SuperInput.inTypeKey (KeyCode.Joystick1Button0),
					new SuperInput.inTypeKey (KeyCode.Space)
				})));
			controller.controls.Add (new SuperInput.ControllerNode ("Button2", new SuperInput.InTypeMulti (//2
				new List<SuperInput.InputNode> {
					new SuperInput.inTypeKey (KeyCode.Joystick1Button1),
					new SuperInput.inTypeKey (KeyCode.LeftShift)
				})));
			controller.controls.Add (new SuperInput.ControllerNode ("Button2", new SuperInput.InTypeMulti (//3
				new List<SuperInput.InputNode> {
					new SuperInput.inTypeKey (KeyCode.Joystick1Button2),
					new SuperInput.inTypeKey (KeyCode.Escape)
				})));
		}
	
		// Update is called once per frame
		void Update () {
			controller.update ();
		}

		public void setUnmapped (SuperInput.ControllerType c) {
			for (int i = 0; i < c.controls.Count; i++) {
				c.controls [i].setInputNode (new SuperInput.inTypeUnassigned ());
			}
		}

		public void setDriven (SuperInput.ControllerType c) {
			for (int i = 0; i < c.controls.Count; i++) {
				c.controls [i].setInputNode (new SuperInput.inTypeDriven ());
			}
		}
	}
}