# README #

### What is this repository for? ###

* Yuge Game

### How do I get set up? ###

* To get started:
*	1) Download Unity (https://store.unity.com/download?ref=personal)
*	2) Clone Repo
*	3) Open Unity Project
*	4) Make sure asset serialization is correct
*		a) Edit -> Project Settings -> Editor
*		b) Version Control = Visible Meta Files
*		c) Asset Serialization = Force Text
*	5) Keep Projects Organized
*		a) Keep individual game in folders
*		b) Keep game assets organized
*		c) Look at Game1 folder for reference
*	6) Use Unity Documentation (https://docs.unity3d.com/ScriptReference/)
